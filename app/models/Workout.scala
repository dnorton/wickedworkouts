package models

import org.joda.time.DateTime

case class Workout(id: Long, workoutType: String, distance: Double, time: Long, createdDate: DateTime, summary: String){
//  def equals(that: Any) = true
}

object Workout {

  var devWorkouts = Set(
    Workout(1234L, "run", 6.2, 45*60L, new DateTime(), "Good 10K Run."),
    Workout(1235L, "run", 10L, (60+15)*60L, new DateTime(), "10 mile run")
  )

  def all(): List[Workout] = this.devWorkouts.toList

  def create(workoutType: String, distance: Long, time: Long, summary: String) {}


}
