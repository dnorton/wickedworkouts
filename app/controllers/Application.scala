package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import models.Workout


object Application extends Controller {

  val workoutForm = Form(
    "workoutType" -> nonEmptyText
  )

  def index = Action {
    Ok("Wicked Workouts - Starting")
  }

  def workouts = Action {
    Ok(views.html.index(Workout.all(), workoutForm))
  }

  def newWorkout = TODO

  def deleteWorkout(id: Long) = TODO
  
}