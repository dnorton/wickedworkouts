package controllers

import play.api.mvc.{Action, Controller}
import models.Workout

object Workouts extends Controller {

  def list = Action { implicit request =>

    val workouts = Workout.all()

    Ok(views.html.workouts.list(workouts))

  }

}
